variable "my_access_key" {
    description = "Access-key-value"
    default = "no_access_key"
}

variable "my_secret_key" {
    description = "Secret-key-aws"
    default = "no_secret_key"
}