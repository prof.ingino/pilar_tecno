resource "aws_instance" "aws_terraform" {
    ami = "ami-07d02ee1eeb0c996c"
    instance_type = "t2.micro"
    key_name = "aws_terraform"
    user_data = file("/home/pingino/Documents/pilar_tecno/aws_terraform/script.sh")
    tags = {
      "Name" = "aws_terraform"
    }
    vpc_security_group_ids = [aws_security_group.sg-terraform.id]

    connection {
      type = "ssh"
      host = self.public_ip
      user = "admin"
      private_key = file("/home/pingino/.ssh/id_rsa_aws")
      timeout = "4m"
    }
}